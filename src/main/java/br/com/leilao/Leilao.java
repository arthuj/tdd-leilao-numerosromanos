package br.com.leilao;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Leilao {

    List<Lance> listaLances = new ArrayList<Lance>();

    public Leilao(List<Lance> listaLances) {
        this.listaLances = listaLances;
    }

    public Leilao() {
    }

    public void adicionarLance(Lance lance) {

        if (this.listaLances.size() >= 1) {
            if (lance.getValorLance() < this.listaLances.get(this.listaLances.size() - 1).getValorLance())
                throw new RuntimeException("O valor do lance não pode ser menor que o lance anterior");

        }

        this.listaLances.add(lance);

        System.out.println("Lance adicionado");
    }


    public List<Lance> getListaLances() {
        return listaLances;
    }

    public void setListaLances(List<Lance> listaLances) {
        this.listaLances = listaLances;
    }
}
