package br.com.leilao;

public class Leiloeiro {

    private  String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public void retornarMaiorLance()
    {
        Lance lanceMaior = new Lance();
        lanceMaior.setValorLance(0);
        if(this.leilao.listaLances.size() > 0) {
            for (Lance lance : this.leilao.listaLances) {
                if (lanceMaior.getValorLance() < lance.getValorLance())
                    lanceMaior = lance;
            }

            System.out.println("O maior lance foi de " + lanceMaior.getValorLance() + " reais, do usuário "
                    + lanceMaior.getUsuario());
        }
        else
            throw  new RuntimeException("Não existem lances");
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
}
