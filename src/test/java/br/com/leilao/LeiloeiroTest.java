package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTest {

    private Leiloeiro leiloeiro;
    private  Leilao leilao;

    @BeforeEach
    public void setUp()
    {
        leiloeiro = new Leiloeiro();
        leilao = new Leilao();
    }

    @Test
    public void testaRetornarMaiorValor()
    {
        Usuario usuario = new Usuario("Arthur",1);
        Lance lance = new Lance(usuario, 300);
        leilao.adicionarLance(lance);
        Usuario usuario2 = new Usuario("Pedro",2);
        Lance lance2 = new Lance(usuario, 400);
        leilao.adicionarLance(lance2);
        Usuario usuario3 = new Usuario("Joao",2);
        Lance lance3 = new Lance(usuario, 500);
        leilao.adicionarLance(lance2);
        leiloeiro.setLeilao(leilao);
        leiloeiro.setNome("LeilaoTeste");
        Assertions.assertDoesNotThrow(()->{leiloeiro.retornarMaiorLance();});
    }

    @Test
    public void testaNaoFelizRetornarMaiorValor()
    {
        Assertions.assertThrows(RuntimeException.class,()-> {leiloeiro.retornarMaiorLance();});
    }
}
