package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTest {

    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public  void setUp()
    {
        leilao = new Leilao();
    }


    @Test
    public void testaAdicionarLanceValoresCorretos()
    {
        Usuario usuario = new Usuario("Arthur",1);
        Lance lance = new Lance(usuario, 300);
        Assertions.assertDoesNotThrow(()->{leilao.adicionarLance(lance);});
    }

    @Test
    public void testaAdicionarVariosValores()
    {
        Usuario usuario = new Usuario("Arthur",1);
        Lance lance = new Lance(usuario, 300);
        leilao.adicionarLance(lance);
        Usuario usuario2 = new Usuario("Pedro",2);
        Lance lance2 = new Lance(usuario, 400);
        leilao.adicionarLance(lance2);
        Usuario usuario3 = new Usuario("Joao",3);
        Lance lance3 = new Lance(usuario, 500);
        leilao.adicionarLance(lance3);

    }

    @Test
    public void testaAdidiconarValorMenor()
    {
        Usuario usuario = new Usuario("Arthur",1);
        Lance lance = new Lance(usuario, 300);
        leilao.adicionarLance(lance);
        Usuario usuario2 = new Usuario("Pedro",2);
        Lance lance2 = new Lance(usuario, 200);

        Assertions.assertThrows(RuntimeException.class,()-> {leilao.adicionarLance(lance2);});
    }


}
